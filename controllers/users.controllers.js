const User = require('../models/user.model')
const Truck = require('../models/truck.model')
const {
    validateHashedPassword,
    hashPassword
} = require('../services/bcryptPasswordService')
const {
    USER_REQUIRED_FIELDS
} = require('../services/constants')
const errorHandler = require('../services/errorHandler')

const getUserInfo = async (req, res, next) => {
    try {
        const {
            _id
        } = req.verifiedUser
        const user = await User.findById(_id, USER_REQUIRED_FIELDS)
        res.status(200).json({
            user
        })
    } catch (error) {
        errorHandler();
    }
}

const deleteUser = async (req, res, next) => {
    try {
        const {
            _id
        } = req.verifiedUser
        await User.findByIdAndDelete(_id)
        await Truck.deleteMany({
            userId: _id
        })
        res.status(200).json({
            message: 'Profile deleted successfully'
        })
    } catch (error) {
        errorHandler();
    }
}

const changeUserPassword = async (req, res, next) => {
    try {
        const {
            _id
        } = req.verifiedUser
        const {
            oldPassword,
            newPassword
        } = req.body
        const user = await User.findById(_id)
        await validateHashedPassword(user.password, oldPassword)
        user.password = await hashPassword(newPassword)
        await user.save()
        res.status(200).json({
            'message': 'Password changed succesfully'
        })
    } catch (error) {
        errorHandler();
    }

}



module.exports = {
    getUserInfo,
    deleteUser,
    changeUserPassword
}