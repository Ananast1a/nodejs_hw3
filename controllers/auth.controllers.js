const jwt = require('jsonwebtoken')
const User = require('../models/user.model')
const CustomError = require('../services/customError')
const getCreatedDate = require('../services/getCreatedDate')
const {validateHashedPassword, hashPassword} = require('../services/bcryptPasswordService')
const { registerUserSchema } = require('../services/joiValidationSchemas');
const SECRET_KEY = process.env.SECRET_KEY;
const errorHandler = require('../services/errorHandler')

const registerUser = async (req, res, next) => {
    try {
      const { email, password, role } = req.body
      const { error } = registerUserSchema.validate({ email, password, role })
      if (error) {
        throw new CustomError(400, error.message)
      }
      const createdDate = getCreatedDate()
      const user = {
        email,
        role,
        username: email,
        created_date: createdDate,
        password: await hashPassword(password)
      }
      await User.create(user)
  
      res.status(200).json({ message: 'Profile created successfully' })
    } catch (error) {
        errorHandler();
    }
}

const loginUser = async (req, res) => {
    try {
        const visitor = req.body
        const user = await User.findOne({
            email: visitor.email
        })
        if (!user) {
            throw new CustomError(400, 'Email not found')
        }
        await validateHashedPassword(user.password, visitor.password)
        const payload = {
            username: user.username,
            _id: user['_id']
        }
        user.token = jwt.sign(payload, SECRET_KEY)
        await user.save()
        res.status(200).json({
            jwt_token: user.token
        })
    } catch (error) {
        errorHandler();
    }
}

module.exports = {
    registerUser,
    loginUser
}