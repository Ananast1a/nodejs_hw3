const { Router } = require('express')
const {
  getUserLoads,
  deleteUserLoad,
  createUserLoad,
  getUserLoad,
  updateUserLoad,
  getUserActiveLoads,
  triggerNextUserLoadState,
  postUserLoad,
  getLoadShippingInfo } = require('../../controllers/loads.controllers')
const {validateDriverRole, validateShipperRole} = require('../../middlewares')

const router = Router()

router.post('/', validateShipperRole, createUserLoad) 
router.get('/', getUserLoads) 
router.get('/active', validateDriverRole, getUserActiveLoads) 
router.patch('/active/state', validateDriverRole, triggerNextUserLoadState) 
router.get('/:id', getUserLoad)
router.put('/:id', validateShipperRole, updateUserLoad) 
router.delete('/:id', validateShipperRole, deleteUserLoad) 
router.post('/:id/post', validateShipperRole, postUserLoad) 
router.get('/:id/shipping_info', validateShipperRole, getLoadShippingInfo) 


module.exports = router