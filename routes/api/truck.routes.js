const { Router } = require('express')
const {
  createUserTruck,
    viewUserTrucks,
    viewUserTruck,
    deleteUserTruck,
    assignUserTruck,
    updateUserTruck } = require('../../controllers/trucks.controller')

const router = Router()

router.post('/', createUserTruck)
router.get('/:id', viewUserTruck)
router.get('/', viewUserTrucks)
router.put('/:id', updateUserTruck)
router.delete('/:id', deleteUserTruck)
router.post('/:id/assign', assignUserTruck)

module.exports = router