const { Schema, model } = require('mongoose')

const user = new Schema(
  {
    password: {
      type: String,
      required: [true, 'Password is required']
    },
    email: {
      type: String,
      required: [true, 'Email is required'],
      unique: true
    },
    username: {
      type: String,
      required: [true, 'Name is required'],
      unique: true
    },
    created_date:
    {
      type: String,
      required: [true, 'Creation date is required']
    },
    role: {
      type: String,
      required: [true, 'Role is required']

    },
    token: {
      type: String,
      default: null
    }
  }
)

const User = model('users', user)

module.exports = User
