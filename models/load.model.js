const { Schema, model } = require('mongoose')
const {LOAD_STATUS} = require('../services/constants')

const load = new Schema(
  {
    created_by: {
      type: String,
      required: [true, 'created_by is required']
    },
    assigned_to: {
      type: String,
      default: ''
    },
    status: {
      type: String,
      default: LOAD_STATUS.NEW

    },
    state: {
      type: String,
      default: ''
    },
    name: {
      type: String,
      required: [true, 'Name is required']
    },
    payload: {
      type: Number,
      required: [true, 'Payload is required']
    },
    pickup_address: {
      type: String,
      required: [true, 'Pickup address is required']
    },
    delivery_address: {
      type: String,
      required: [true, 'Delivery address is required']
    },
    dimensions: {
      width: {
        type: Number,
        required: [true, 'Width is required']
      },
      length: {
        type: Number,
        required: [true, 'Length is required']
      },
      height: {
        type: Number,
        required: [true, 'Height is required']
      }

    },
    logs: {
      type: Array,
      of: {
        message: {
          type: String,
          required: [true, 'Log message is required']
        },
        time: {
          type: String,
          required: [true, 'Message time is required']
        }
      },
      default: []
    },
    created_date: {
      type: String,
      required: [true, 'Creation date is required']
    }
  }
)

const Load = model('loads', load)

module.exports = Load