const { Schema, model } = require('mongoose')

const truck = new Schema(
  {
    created_by: {
      type: String,
      required: [true, 'created_by is required']
    },
    assigned_to: {
      type: String,
      default: ''
    },
    created_date:
    {
      type: String,
      required: [true, 'Creation date is required']
    },
    type: {
      type: String,
      required: [true, 'type date is required']
    },
    status: {
      type: String,
      default: 'IS'
    }
  }
)

const Truck = model('trucks', truck)

module.exports = Truck