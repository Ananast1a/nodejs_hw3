const {TRUCK_SIZES} = require('./constants')
const {TRUCK_STATUS} = require('./constants')
const Truck = require('../models/truck.model')
const CustomError = require('./customError')

const getTruckForLoad = (load) => {
  const sizes = []
  for (let key in TRUCK_SIZES) {
    const loadFits = TRUCK_SIZES[key].payload > load.payload
      && TRUCK_SIZES[key].dimensions.width > load.dimensions.width
      && TRUCK_SIZES[key].dimensions.height > load.dimensions.height
      && TRUCK_SIZES[key].dimensions.length > load.dimensions.length
    if(loadFits) {
      sizes.push(key)
      }
  }
  return sizes
}

const findTrackForLoad = async (load) => {
  const truckTypes = getTruckForLoad(load)
  if (truckTypes.length === 0) {
    throw new CustomError(400, 'Load is too big for any of trucks')
  }
  const condition = {
    type: {$in: truckTypes},
    status: TRUCK_STATUS.IS,
    assigned_to: {$ne: ''}
  }
  return await Truck.findOne(condition)
}

module.exports = {
  getTruckForLoad,
  findTrackForLoad
}