const Joi = require('joi')
const { LOAD_STATUS, LOADS_PAGINATION_OPTS, TRUCK_TYPE, USER_ROLE } = require('./constants')

const newLoadSchema = Joi.object({
    name: Joi.string().min(1).required(),
    payload: Joi.number().positive().required(),
    pickup_address: Joi.string().min(1).required(),
    delivery_address: Joi.string().min(1).required(),
    dimensions: Joi.object({
      width: Joi.number().positive().required(),
      length: Joi.number().positive().required(),
      height: Joi.number().positive().required()
    })
  })
  
  const updateLoadSchema = Joi.object({
    name: Joi.string().min(1),
    payload: Joi.number().positive(),
    pickup_address: Joi.string().min(1),
    delivery_address: Joi.string().min(1),
    dimensions: Joi.object({
      width: Joi.number().positive(),
      length: Joi.number().positive(),
      height: Joi.number().positive()
    }).min(1)
  })
  
  const getLoadsSchema = Joi.object({
    status: Joi.string().pattern(new RegExp(`${Object.values(LOAD_STATUS).join('|')}`)).optional(),
    limit: Joi.number().positive().max(LOADS_PAGINATION_OPTS.LIMIT.max).optional(),
    offset: Joi.number().positive().optional()
  
  })

  const truckTypeSchema = Joi.object({
    type: Joi.string().pattern(new RegExp(`${Object.values(TRUCK_TYPE).join('|')}`)).required()
  })

  const registerUserSchema = Joi.object({
    email: Joi.string().pattern(/^([a-zA-Z0-9.]+)@([a-zA-Z0-9_.]+)\.([a-zA-Z]{2,5})$/).required(),
    password: Joi.string().min(1).required(),
    role: Joi.string().pattern(new RegExp(`${USER_ROLE.DRIVER}|${USER_ROLE.SHIPPER}`)).required()
  })
  
  const loginUserSchema = Joi.object({
    email: Joi.string().min(1).required(),
    password: Joi.string().min(1).required()
  })
  
  const changeUserPasswordSchema = Joi.object({
    oldPassword: Joi.string().min(1).required(),
    newPassword: Joi.string().min(1).invalid(Joi.ref('oldPassword')).required()
  })
  
  const forgotPasswordSchema = Joi.object({
    email: Joi.string().min(1).required()
  })



const joiValidationService = (schema, obj) => {
    const { error } = schema.validate(obj)
      if (error) {
        throw new CustomError(400, error.message)
      }
  }
  
  module.exports = {
    registerUserSchema,
    loginUserSchema,
    truckTypeSchema,
    forgotPasswordSchema,
    newLoadSchema,
    getLoadsSchema,
    updateLoadSchema,
    joiValidationService
  }