const { USER_ROLE, LOAD_STATUS } = require('./constants')
const CustomError = require('./customError')

const driverAllowedStatus = [LOAD_STATUS.ASSIGNED, LOAD_STATUS.SHIPPED]
const shipperAllowedStatus = [LOAD_STATUS.NEW, LOAD_STATUS.POSTED]

const getAllowedStatus = (status, arr) => {
  if (!status) {
    return {$in: arr}
  }

  if (!arr.includes(status)) {
    throw new CustomError(400, `Status ${status} is not allowed for current role`)
  }
  return status
}

const filterByRole = (role, id, status) => {
  return role === USER_ROLE.DRIVER
      ? {
        status: getAllowedStatus(status, driverAllowedStatus),
        assigned_to: id
      }
      : {
        status: getAllowedStatus(status, shipperAllowedStatus),
        created_by: id
    }
}



module.exports = {
  filterByRole
}