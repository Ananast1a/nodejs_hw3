const errorHandler = (error) => {
  if (!error.status) {
    res.status(500).json({ message: 'Internal server error' })
  } else {
    res.status(400).json({ message: error.message })
  }
}

module.exports = {errorHandler}